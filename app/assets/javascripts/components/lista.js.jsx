var ItemLista  = React.createClass ({
    render: function(){
        return(
            <option value={this.props.dato.mensaje}>{this.props.dato.Id_Lay} mostrar {this.props.dato.mensaje}</option>
        );
    }
});

var ItemLay  = React.createClass ({
    render: function(){
        return(
            <option value={this.props.dato.Id_Campo}>{this.props.dato.Nombre_Campo}</option>
        );
    }
});

var ItemLayLista  = React.createClass ({
    render: function(){
        return(
                <p>ID CAMPO {this.props.dato.Id_Campo}
                {this.props.dato.Nombre_Campo}
                Longitud: {this.props.dato.Longitud}</p>
        );
    }
});

var CrearLista = React.createClass ({
    getInitialState: function(){
        return {datos: this.props.bitacora, layouts: this.props.lay};
    },

    render: function(){
     var tempLista = this.state.datos.map( function (valor) {
          return <ItemLista dato={valor} />;
      });

      var tempLay = this.state.layouts.map( function (valor) {
          return <ItemLayLista dato={valor} />;
      });

      return(
          <div id="dragable" className="ui-widget-content">
            {tempLay}
          </div>
      );
    }
});

